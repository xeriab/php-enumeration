<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

/**
 * The interface implemented by Java-style enumeration instances with a value.
 *
 * @api
 */
interface ValueMultitonInterface extends MultitonInterface
{
    /**
     * Returns the value of this member.
     *
     * @api
     *
     * @return mixed The value of this member.
     */
    public function value();

    /**
     * Returns the name of this member.
     *
     * @api
     *
     * @return mixed The name of this member.
     */
    public function name();
}
