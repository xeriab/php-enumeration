<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

/**
 * The interface implemented by C++ style enumeration instances.
 *
 * @api
 */
interface EnumerationInterface extends ValueMultitonInterface
{
}
