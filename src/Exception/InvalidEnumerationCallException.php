<?php

declare(strict_types=1);

namespace Xeriab\Enumeration\Exception;

use LogicException;
use Throwable;
use function sprintf;

class InvalidEnumerationCallException extends LogicException
{
    /**
     *
     * @var string
     */
    private $enumName;

    /**
     *
     * @var string
     */
    private $enumClass;

    public function __construct(string $enumName = null, string $enumClass = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            sprintf(
                'Member with key equal to \'%s\' can not be called from class \'%s\'',
                $enumName,
                $enumClass
            ),
            $code,
            $previous
        );

        $this->enumName = $enumName;
        $this->enumClass = $enumClass;
    }

    /**
     *
     * @return string
     */
    public function enumName(): string
    {
        return $this->enumName;
    }

    /**
     *
     * @return string
     */
    public function enumClass(): string
    {
        return $this->enumClass;
    }
}
