<?php

declare(strict_types=1);

namespace Xeriab\Enumeration\Exception;

use LogicException;
use Throwable;
use function sprintf;

class EnumerationNotFoundException extends LogicException
{
    /**
     *
     * @var string
     */
    private $missingEnumName;

    /**
     *
     * @var string
     */
    private $enumClass;

    public function __construct(string $missingEnumName = null, string $enumClass = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Enum kry/element %s missing in %s', $missingEnumName, $enumClass), $code, $previous);
        $this->missingEnumName = $missingEnumName;
        $this->enumClass = $enumClass;
    }

    /**
     *
     * @return string
     */
    public function missingEnumName(): string
    {
        return $this->missingEnumName;
    }

    /**
     *
     * @return string
     */
    public function enumClass(): string
    {
        return $this->enumClass;
    }
}
