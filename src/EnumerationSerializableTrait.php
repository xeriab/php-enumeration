<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

use RuntimeException;
use LogicException;

/**
 * Enumeration serializable trait.
 *
 * @since 0.1
 */
trait EnumerationSerializableTrait
{
    /**
     * The method will be defined by \Xeriab\Enumeration\Enum.
     *
     * @return null|bool|int|float|string
     */
    abstract public function getValue();

    /**
     * Serialized the value of the enumeration
     * This will be called automatically on `serialize()` if the enumeration implements the `Serializable` interface.
     *
     * @return string
     */
    public function serialize(): string
    {
        return \serialize($this->getValue());
    }

    /**
     * Unserializes a given serialized value and push it into the current instance
     * This will be called automatically on `unserialize()` if the enumeration implements the `Serializable` interface.
     *
     * @param string $serialized
     *
     * @throws RuntimeException On an unknown or invalid value
     * @throws LogicException   On changing numeration value by calling this directly
     */
    public function unserialize($serialized)
    {
        $value = \unserialize($serialized);
        $constants = self::getConstants();
        $name = \array_search($value, $constants, true);

        if (false === $name) {
            $message = \is_scalar($value)
                ? 'Unknown value '.\var_export($value, true)
                : 'Invalid value of type '.(\is_object($value) ? \get_class($value): \gettype($value));
            throw new RuntimeException($message);
        }

        $class = \get_class($this);
        $enumerator = $this;
        $closure = function () use ($class, $name, $value, $enumerator) {
            if (null !== $value && null !== $this->enumValue) {
                throw new LogicException('Do not call this directly - please use unserialize($enum) instead');
            }

            $this->enumValue = $value;

            if (!isset(self::$enumInstances[$class][$name])) {
                self::$enumInstances[$class][$name] = $enumerator;
            }
        };

        $closure = $closure->bindTo($this, self::class);
        $closure();
    }

    public function jsonSerialize()
    {
        return $this::getConstList();
    }

    public function yamlSerialize()
    {
        return $this::getConstList();
    }
}
