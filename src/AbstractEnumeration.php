<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

use Xeriab\Enumeration\Exception\InvalidEnumerationCallException;
use Xeriab\Enumeration\Exception\EnumerationNotFoundException;
use Xeriab\Enumeration\Exception\UndefinedMemberException;
use ReflectionClass;
use Iterator;
use ArrayIterator;
use function get_called_class;

/**
 * Abstract base class for C++ style enumerations.
 *
 * @api
 */
abstract class AbstractEnumeration extends AbstractValueMultiton implements EnumerationInterface
{
    /**
     * Default enum.
     *
     * @var string
     */
    protected static $defaultKey = '__DEFAULT';

    /**
     * Default enum.
     *
     * @var mixed
     */
    protected static $default = null;

    /**
     * Initializes the members of this enumeration based upon its class
     * constants.
     *
     * @param bool $includeDefault
     *
     * Each constant becomes a member with a string key equal to the constant's
     * name, and a value equal to that of the constant's value.
     */
    final protected static function initializeMembers(
        bool $includeDefault = false
    ): void {
        $ordinal = 0;

        $refClass = new ReflectionClass(get_called_class());
        $constants = $refClass->getConstants();

        if ($includeDefault) {
            self::$default = $constants[self::$defaultKey];
        } else {
            unset($constants[self::$defaultKey]);
        }

        // unset($constants[self::$defaultKey]);

        foreach ($constants as $key => $value) {
            if ($includeDefault && $key == self::$defaultKey) {
                continue;
            }

            new static($key, $value, $ordinal++);
        }
    }

    /**
     *
     * @param bool $includeDefault
     *
     * @return array
     */
    final public static function getConstList(bool $includeDefault = false): array
    {
        $constants = [];

        foreach (static::members($includeDefault) as $key => $value) {
            $constants[$key] = $value->getValue();
        }

        return $constants;
    }

    final public static function getKeys(bool $includeDefault = false): array
    {
        $constants = static::getConstList($includeDefault);
        return \array_keys($constants);
    }

    final public static function keys(bool $includeDefault = false): array
    {
        $constants = static::getConstList($includeDefault);
        return \array_keys($constants);
    }

    final public static function getNames(bool $includeDefault = false): array
    {
        $constants = static::getConstList($includeDefault);
        return \array_keys($constants);
    }

    final public static function names(bool $includeDefault = false): array
    {
        $constants = static::getConstList($includeDefault);
        return \array_keys($constants);
    }

    final public static function getValues(bool $includeDefault = false): array
    {
        $constants = static::getConstList($includeDefault);
        return \array_values($constants);
    }

    final public static function values(bool $includeDefault = false): array
    {
        return static::getValues($includeDefault);
    }

    final public static function getUniqueValues(bool $includeDefault = false): array
    {
        return \array_unique(static::values($includeDefault));
    }

    final public static function uniqueValues(bool $includeDefault = false): array
    {
        return \array_unique(static::values($includeDefault));
    }

    /**
     *
     * @return mixed
     */
    final public static function default()
    {
        // return static::__DEFAULT;
        return static::byValue(static::__DEFAULT);
    }

    /**
     *
     * @return mixed
     */
    final public static function getDefault()
    {
        return static::default();
    }

    final public static function byOrdinal(
        $value = null,
        bool $isCaseSensitive = false
    ): ?self {
        $result = null;

        try {
            $result = static::memberBy('ordinal', $value, $isCaseSensitive);
        } catch (UndefinedMemberException $ex) {
            $result = null;
        }

        return $result;
    }

    final public static function byValue(
        $value = null,
        bool $isCaseSensitive = false
    ): ?self {
        $result = null;

        try {
            $result = static::memberBy('value', $value, $isCaseSensitive);
        } catch (UndefinedMemberException $ex) {
            $result = null;
        }

        return $result;
    }

    final public static function byName(
        string $name = null,
        bool $isCaseSensitive = false
    ): ?self {
        $result = null;

        try {
            $result = static::memberBy('key', $name, $isCaseSensitive);
        } catch (UndefinedMemberException $ex) {
            $result = null;
        }

        return $result;
    }

    final public static function byKey(
        string $key = null,
        bool $isCaseSensitive = false
    ): ?self {
        $result = null;

        try {
            $result = static::memberBy('key', $key, $isCaseSensitive);
        } catch (UndefinedMemberException $ex) {
            $result = null;
        }

        return $result;
    }

    final public static function by(
        $predict = null,
        bool $isCaseSensitive = false
    ): ?self {
        $type   = null;
        $result = null;

        if ($predict instanceof EnumerationInterface) {
            $predict = $predict->key();
            $type = 'key';
        }

        if (\is_string($predict)) {
            $type = 'key';
        } elseif (\is_numeric($predict) or \is_integer($predict)) {
            $type = 'value';
        }

        try {
            $result = static::memberBy($type, $predict, $isCaseSensitive);
        } catch (UndefinedMemberException $ex) {
            // DEBUG($ex);
            $result = null;
        }

        return $result;
    }

    final public static function getSqlEnumDeclaration(
        string $comment = null,
        bool $includeDefault = false
    ): string {
        $constants = static::getConstList($includeDefault);
        $default = '';

        if (!\is_null(static::default())) {
            $default = \sprintf('DEFAULT \'%s\' ', static::default()->key());
        } else {
            $default = '';
        }

        // return sprintf(
        //     'ENUM(\'%s\')%sCOMMENT "%s"',
        //     \implode("', '", \array_keys($constants)),
        //     $default,
        //     // $comment ?: parseClassName(static::class)['classname'] . ' Enum.'
        //     $comment ?: parseClassName(static::class)['classname']
        // );

        return \sprintf(
            'ENUM(\'%s\')',
            \implode("', '", \array_keys($constants))
        );
    }

    /**
     *
     * @return Iterator|static[] Enum items in order they are defined
     */
    final public static function iterator(): Iterator
    {
        return new ArrayIterator(static::members());
    }

    /**
     *
     * @throws LogicException Enum are not cloneable
     *                        because instances are implemented as singletons
     */
    private function __clone()
    {
        throw new \LogicException('Enum are not cloneable');
    }

    /**
     *
     * @throws LogicException Enum are not serializable
     *                        because instances are implemented as singletons
     */
    final public function __sleep()
    {
        throw new \LogicException('Enum are not serializable');
    }

    /**
     *
     * @throws LogicException Enum are not serializable
     *                        because instances are implemented as singletons
     */
    final public function __wakeup()
    {
        throw new \LogicException('Enum are not serializable');
    }

    /**
     *
     * @throws Throwable
     */
    final public static function __set_state()
    {
        throw new \LogicException('Serialization/Deserialization of Enum is not allowed');
    }

    /**
     * Maps static method calls to members.
     *
     * @api
     *
     * @param string $key       The string key associated with the member.
     * @param array  $arguments Ignored.
     *
     * @return static                            The member associated with the given string key.
     * @throws UndefinedMemberExceptionInterface If no associated member is found.
     */
    final public static function __callStatic($key, array $arguments): self
    {
        if (\count($arguments) > 0) {
            throw new \InvalidArgumentException(
                \sprintf(
                    'No argument must be provided when calling %s::%s',
                    static::class,
                    $key
                )
            );
        }

        if (self::$defaultKey === $key) {
            throw new InvalidEnumerationCallException($key, static::class);
        }

        return static::memberByKey($key);
    }

    /**
     * toString.
     *
     * @return string
     *
     * @since              0.1
     * @codeCoverageIgnore
     */
    public function toString(): string
    {
        return $this->key();
    }

    /**
     * __toString.
     *
     * @return string
     *
     * @since              0.1
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return $this->key();
    }
}
