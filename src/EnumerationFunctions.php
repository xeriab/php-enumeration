<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

use LogicException;
use ReflectionClass;
use ReflectionException;

use function get_class;
use function gettype;
use function is_object;
use function is_string;
use function preg_match;
use function sprintf;

function assertValidNamePattern(string $name = null): void
{
    $pattern = '/^[a-zA-Z_][a-zA-Z_0-9]*$/i';

    if (preg_match($pattern, $name)) {
        return;
    }

    throw new LogicException(sprintf('Element name "%s" does not match pattern %s', $name, $pattern));
}

/**
 *
 * @param string $class
 *
 * @throws ReflectionException
 */
function assertEnumClassIsAbstract(string $class = null): void
{
    if ((new ReflectionClass($class))->isAbstract()) {
        return;
    }

    throw new LogicException(sprintf('Enum %s must be declared as abstract', $class));
}

function assertValidEnumCollection(string $class = null, array $enumCollection = [], string $enumClass = null): void
{
    foreach ($enumCollection as $elementName => $object) {
        assertElementNameIsString($class, $elementName);
        assertValidNamePattern($elementName);
        assertValidEnumElementObjectType($class, $object, $enumClass);
    }
}

function assertElementNameIsString(string $class = null, $name = null): void
{
    if (is_string($name)) {
        return;
    }

    if (is_object($name)) {
        $formattedElementName = sprintf('(object instance of %s)', get_class($name));
    } else {
        $formattedElementName = $name;
    }

    throw new LogicException(
        sprintf('Element name %s in enum %s is not valid', $formattedElementName, $class)
    );
}

function assertValidEnumElementObjectType(string $class = null, $object = null, string $enumClass = null): void
{
    if ($object instanceof $class) {
        return;
    }

    if (is_object($object)) {
        $resolvedType = 'an instance of ' . get_class($object);
    } else {
        $resolvedType = gettype($object);
    }

    throw new LogicException(
        sprintf(
            'Enum element object in enum %s must be an instance of %s (%s received)',
            $enumClass,
            $class,
            $resolvedType
        )
    );
}

//

// class EnumerationFunctions {}
