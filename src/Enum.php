<?php

declare(strict_types=1);

namespace Xeriab\Enumeration;

/**
 * Enum class.
 *
 * @since 0.1
 */
abstract class Enum extends AbstractEnumeration
{
}
