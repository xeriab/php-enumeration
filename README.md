<center>![Enumeration](.gitlab/enumeration-logo.png "Enumeration")</center>

Enumeration
===========

PHP enumeration library.

Resources
---------

  * [Documentation](https://xeriab.gitlab.com/docs/php-enumeration/index.html)
  * [Contributing](https://xeriab.gitlab.com/docs/contributing/index.html)
  * [Changes log](CHANGES.md).
  * [Report issues](https://gitlab.com/xeriab/php-enumeration/issues) and
    [Send Pull/Merge Requests](https://gitlab.com/xeriab/php-enumeration/merge_requests)
    in the [main enumeration repository](https://gitlab.com/xeriab/php-enumeration)

## Copyright and License

Copyright [Xeriab Nabil](#). under the [MIT license](LICENSE).
